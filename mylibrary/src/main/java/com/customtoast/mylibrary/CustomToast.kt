package com.customtoast.mylibrary

import android.content.Context
import android.widget.Toast

object CustomToast {
    fun showCustomToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}