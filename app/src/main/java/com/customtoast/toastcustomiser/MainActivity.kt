package com.customtoast.toastcustomiser

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.customtoast.mylibrary.CustomToast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        CustomToast.showCustomToast(application, "Hello from Parent App")
    }
}